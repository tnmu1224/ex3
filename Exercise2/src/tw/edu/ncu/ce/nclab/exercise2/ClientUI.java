package tw.edu.ncu.ce.nclab.exercise2;

import java.awt.AWTEvent;
import java.awt.Button;
import java.awt.Dimension;
import java.awt.Label;
import java.awt.List;
import java.awt.Rectangle;
import java.awt.TextField;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.WindowEvent;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class ClientUI extends JFrame {
	private JPanel contentPane;
	private TextField hostName = new TextField();
	private TextField portNumber = new TextField();
	private Label hostLabel = new Label();
	private Label portLabel = new Label();
	private Button startButton = new Button();
	private Button endButton = new Button();
	private TextField numberInputField = new TextField();
	private Label numberInputLabel = new Label();
	private List outputList = new java.awt.List();
	private Label outputLabel = new Label();
	private Button connectButton = new Button();
	private Socket socketToServer;// would be used
	private DataOutputStream output = null;
	private DataInputStream input = null;

	// Construct the frame
	public ClientUI() {
		enableEvents(AWTEvent.WINDOW_EVENT_MASK);
		try {
			initComponent();
			initialFrame();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// Frame initialization
	private void initialFrame() {
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		Dimension frameSize = this.getSize();
		if (frameSize.height > screenSize.height) {
			frameSize.height = screenSize.height;
		}
		if (frameSize.width > screenSize.width) {
			frameSize.width = screenSize.width;
		}
		this.setLocation((screenSize.width - frameSize.width) / 2,
				(screenSize.height - frameSize.height) / 2);
		this.setVisible(true);
	}

	// Component initialization
	private void initComponent() throws Exception {

		contentPane = (JPanel) this.getContentPane();
		hostName.setBounds(new Rectangle(23, 55, 146, 27));
		contentPane.setLayout(null);
		this.setSize(new Dimension(400, 300));
		this.setTitle("Socket Client");
		portNumber.setBounds(new Rectangle(23, 128, 148, 27));
		portNumber.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {

			}
		});
		hostLabel.setText("Host_name");
		hostLabel.setBounds(new Rectangle(23, 24, 147, 29));
		portLabel.setText("Port_number");
		portLabel.setBounds(new Rectangle(23, 95, 148, 28));
		startButton.setLabel("Start");
		startButton.setBounds(new Rectangle(299, 155, 87, 31));
		startButton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				startActionPerformed(e);
			}
		});
		endButton.setLabel("End");
		endButton.setBounds(new Rectangle(207, 194, 179, 31));
		endButton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				endActionPerformed(e);
			}
		});
		numberInputField.setBounds(new Rectangle(24, 193, 147, 31));
		numberInputLabel.setText("Input_NUM");
		numberInputLabel.setBounds(new Rectangle(23, 164, 144, 24));
		outputList.setBounds(new Rectangle(207, 56, 176, 87));
		outputLabel.setText("Output");
		outputLabel.setBounds(new Rectangle(206, 24, 175, 27));
		connectButton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				connectActionPerformed(e);
			}
		});
		connectButton.setBounds(new Rectangle(207, 154, 88, 31));
		connectButton.setLabel("Connect");
		contentPane.add(numberInputField, null);
		contentPane.add(numberInputLabel, null);
		contentPane.add(portNumber, null);
		contentPane.add(portLabel, null);
		contentPane.add(hostName, null);
		contentPane.add(hostLabel, null);
		contentPane.add(outputList, null);
		contentPane.add(outputLabel, null);
		contentPane.add(endButton, null);
		contentPane.add(connectButton, null);
		contentPane.add(startButton, null);
	}

	// Could be used
	private void showWarningMessageDialog(String message) {
		JOptionPane.showMessageDialog(null, message, "Alert",
				JOptionPane.WARNING_MESSAGE);
	}

	// Overridden so we can exit when window is closed
	protected void processWindowEvent(WindowEvent e) {
		super.processWindowEvent(e);
		if (e.getID() == WindowEvent.WINDOW_CLOSING) {
			// System.exit(0);

			dispose();
		}
	}

	private void endActionPerformed(ActionEvent e) {
		// add code
		try {
			output.close();
			input.close();
			socketToServer.close();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		output = null;
		input = null;
		socketToServer = null;

	}

	private void startActionPerformed(ActionEvent e) {

		if(hasConnected()){
			// add code
			try {
				int number = Integer.parseInt(numberInputField.getText());

				output.writeInt(number);
				output.flush();
				outputList.add(input.readInt() + "");
			} catch (NumberFormatException e1) {
				showWarningMessageDialog("請輸入數字");
			} catch (IOException e1) {
				showWarningMessageDialog(e1.getMessage());
			}
		}else{
			showWarningMessageDialog("請先建立連線");
		}

		
	}

	private boolean hasConnected() {
		if (socketToServer == null) {
			return false;
		} else {
			return true;
		}
	}

	private void connectActionPerformed(ActionEvent e) {

		if (!hasConnected()) {
			try {
				initialSocket();
			} catch (UnknownHostException e1) {
				showWarningMessageDialog("UnknownHost!");
			} catch (IOException e1) {

				showWarningMessageDialog(e1.getMessage());
			}
		}

	}

	private void initialSocket() throws UnknownHostException, IOException {

		String remoteIP = hostName.getText();
		int port = Integer.parseInt(portNumber.getText());
		socketToServer = new Socket(remoteIP, port);

		output = new DataOutputStream(socketToServer.getOutputStream());
		input = new DataInputStream(socketToServer.getInputStream());

	}

}