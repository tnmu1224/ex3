package tw.edu.ncu.ce.nclab.exercise2;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {

	private int portNumber;

	public Server(int portNumber) {
		this.portNumber = portNumber;
		startServer();

	}

	private void startServer() {
		try {

			ServerSocket serverSocket = new ServerSocket(portNumber);

			while (true) {

				Socket client = serverSocket.accept();
				System.out.println("New client:"
						+ client.getRemoteSocketAddress().toString());

				new Thread(new SocketSrvThread(client)).start();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		new Server(5000);
	}

}

class SocketSrvThread implements Runnable {
	Socket socketToClient;

	public SocketSrvThread(Socket s) {
		socketToClient = s;

	}

	public void run() {

		try (DataOutputStream output = new DataOutputStream(
				socketToClient.getOutputStream());
				DataInputStream input = new DataInputStream(
						socketToClient.getInputStream());) {

			int number;

			try {
				while (true) {

					number = input.readInt();
					number = number + 1;
					output.writeInt(number);
					output.flush();

				}
			} catch (EOFException e) {
				System.out.println("error:" + e.getMessage());
			}

			output.close();
			input.close();
			socketToClient.close();
		} catch (Exception e) {
			System.out.println("error2:" + e.getMessage());
		}

	}
}